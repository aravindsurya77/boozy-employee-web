import React from "react";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs, { Dayjs } from "dayjs";

interface DateRangeSelectorProps {
    startDate: Dayjs;
    endDate: Dayjs;
    updateStartDate: (date: Dayjs) => void;
    updateEndDate: (date: Dayjs) => void;
}

// May be put Date range in the Tabs component
const DateRangeSelector = ({ updateStartDate, updateEndDate, startDate, endDate } : DateRangeSelectorProps) => {
    const today = dayjs(new Date().toString());
    return (
        <div style={{ display: "flex", flexDirection: "column", marginBottom: '20px' }}>
            <div>Please select the Date Range:</div>
            <div style={{ display: "flex", flexDirection: "row", margin: '10px' }}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                        label="Start Date"
                        value={startDate}
                        // @ts-ignore
                        onChange={(newValue) => updateStartDate(newValue)}
                        maxDate={endDate}
                        minDate={dayjs(new Date('2024-02-28').toString())}
                    />
                    <div style={{width: '25px'}}/>
                    <DatePicker
                        label="End Date"
                        minDate={startDate}
                        value={endDate}
                        // @ts-ignore
                        onChange={(newValue) => updateEndDate(newValue)}
                        maxDate={today}
                    />
                </LocalizationProvider>
            </div>

        </div>
    );
};

export default DateRangeSelector;