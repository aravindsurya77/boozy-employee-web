import * as React from 'react';
import {pieArcLabelClasses, PieChart} from '@mui/x-charts/PieChart';
import { DrinkerStatsRecord } from "./Leaderboard";
import { TapIds } from "./DrinksList";
import { brown, deepOrange, green, lightBlue, orange, pink, purple, red, yellow } from "@mui/material/colors";

interface MyDrinkStatsChartProps {
    drinkerStats: DrinkerStatsRecord;
}
const MyDrinksStatsChart = ({drinkerStats} : MyDrinkStatsChartProps) => {

    // @ts-ignore
    const dataForChart = [];
    Object.keys(drinkerStats).forEach(key => {
        switch (key) {
            case TapIds.SECOND_FLOOR_BEER_TAP:
                dataForChart.push({
                    id: TapIds.SECOND_FLOOR_BEER_TAP,
                    value: drinkerStats[TapIds.SECOND_FLOOR_BEER_TAP],
                    label: '2F Beer Tap',
                    color: purple[500]
                });
                break;
            case TapIds.SECOND_FLOOR_WINE_TAP:
                dataForChart.push({
                    id: TapIds.SECOND_FLOOR_WINE_TAP,
                    value: drinkerStats[TapIds.SECOND_FLOOR_WINE_TAP],
                    label: '2F Wine Tap',
                    color: lightBlue[700]
                });
                break;
            case TapIds.THIRD_FLOOR_TAP_1_LAGER:
                dataForChart.push({
                    id: TapIds.THIRD_FLOOR_TAP_1_LAGER,
                    value: drinkerStats[TapIds.THIRD_FLOOR_TAP_1_LAGER],
                    label: '3F Tap1 Lager',
                    color: orange[500]
                });
                break;
            case TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW:
                dataForChart.push({
                    id: TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW,
                    value: drinkerStats[TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW],
                    label: '3F Tap2 Local Brew',
                    color: green[500]
                });
                break;
            case TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW:
                dataForChart.push({
                    id: TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW,
                    value: drinkerStats[TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW],
                    label: '3F Tap3 Local Brew',
                    color: deepOrange[500]
                });
                break;
            case TapIds.THIRD_FLOOR_TAP_4_GUINNESS:
                dataForChart.push({
                    id: TapIds.THIRD_FLOOR_TAP_4_GUINNESS,
                    value: drinkerStats[TapIds.THIRD_FLOOR_TAP_4_GUINNESS],
                    label: '3F Tap4 Guinness',
                    color: brown[600]
                });
                break;
            case TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE:
                dataForChart.push({
                    id: TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE,
                    value: drinkerStats[TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE],
                    label: '3F Tap5 White Wine',
                    color: pink[400]
                });
                break;
            case TapIds.THIRD_FLOOR_TAP_6_RED_WINE:
                dataForChart.push({
                    id: TapIds.THIRD_FLOOR_TAP_6_RED_WINE,
                    value: drinkerStats[TapIds.THIRD_FLOOR_TAP_6_RED_WINE],
                    label: '3F Tap6 Red Wine',
                    color: red[900]
                });
                break;
            case TapIds.SCOTCH_GLASS:
                dataForChart.push({
                    id: TapIds.SCOTCH_GLASS,
                    value: drinkerStats[TapIds.SCOTCH_GLASS],
                    label: 'Personal Scotch',
                    color: yellow[700]
                });
                break;
        }
    });

    const chartSetting = {
        margin: { top: 80, bottom: 50, left: 17 }
    };

    return (
        <PieChart
            series={[{
                // @ts-ignore
                data: dataForChart,
                highlightScope: { faded: 'global', highlighted: 'item' },
                faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
                arcLabel: (item) => `${item.value}`,
                arcLabelMinAngle: 25,
                arcLabelRadius: 75,
                innerRadius: 14,
                outerRadius: 150,
                paddingAngle: 1,
                cornerRadius: 5,
                startAngle: -180,
                endAngle: 180,
                cx: 150,
                cy: 150,
            }]}
            sx={{
                [`& .${pieArcLabelClasses.root}`]: {
                    fill: 'white',
                    fontWeight: 'bold',
                },
            }}
            slotProps={{
                legend: {
                    direction: 'row',
                    position: { vertical: 'top', horizontal: 'middle' },
                    padding: 0,
                    itemMarkWidth: 5,
                    itemMarkHeight: 5,
                    markGap: 5,
                    itemGap: 5,
                    labelStyle: {
                        fontSize: 10,
                    },
                }
            }}
            width={350}
            height={400}
            {...chartSetting}
        />
    );
};

export default MyDrinksStatsChart;