import React from "react";
import { BarChart } from "@mui/x-charts/BarChart";
import { DrinkerStatsRecord } from "./Leaderboard";
import {TapIds} from "./DrinksList";
import { axisClasses } from '@mui/x-charts';
import {brown, deepOrange, green, lightBlue, orange, pink, purple, red, yellow} from "@mui/material/colors";

interface DrinkStatsForAllDrinkersGraphProps {
    drinkStatsPerDrinker: Array<DrinkerStatsRecord>
}

const DrinkStatsPerDrinkerGraph = ({ drinkStatsPerDrinker }: DrinkStatsForAllDrinkersGraphProps) => {

    const chartSetting = {
        xAxis: [
            {
                label: 'Number of Drinks',
            },
        ],
        width: 350,
        height: 1000,
        sx: {
            [`.${axisClasses.left} .${axisClasses.label}`]: {
                transform: 'translate(-20px, 0)',
            },
        },
        margin: { top: 100, bottom: 50, left: 62 }
    };

    return (
        <BarChart
            // @ts-ignore
            dataset={drinkStatsPerDrinker}
            yAxis={[{ scaleType: 'band', dataKey: 'drinkerName' }]}
            layout="horizontal"
            series={[
                // TODO: use TapNames
                { dataKey: TapIds.SECOND_FLOOR_BEER_TAP, label: '2F Beer Tap', color: purple[500] },
                { dataKey: TapIds.SECOND_FLOOR_WINE_TAP, label: '2F Wine Tap', color: lightBlue[700] },
                { dataKey: TapIds.THIRD_FLOOR_TAP_1_LAGER, label: '3F Tap1 Lager', color: orange[500] },
                { dataKey: TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW, label: '3F Tap2 Local Brew', color: green[500] },
                { dataKey: TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW, label: '3F Tap3 Local Brew', color: deepOrange[500] },
                { dataKey: TapIds.THIRD_FLOOR_TAP_4_GUINNESS, label: '3F Tap4 Guinness', color: brown[600] },
                { dataKey: TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE, label: '3F Tap5 White Wine', color: pink[400] },
                { dataKey: TapIds.THIRD_FLOOR_TAP_6_RED_WINE, label: '3F Tap6 Red Wine', color: red[900] },
                { dataKey: TapIds.SCOTCH_GLASS, label: 'Personal Scotch', color: yellow[700] },
            ]}
            {...chartSetting}
            slotProps={{
                legend: {
                    itemMarkWidth: 5,
                    itemMarkHeight: 5,
                    markGap: 5,
                    itemGap: 5,
                    labelStyle: {
                        fontSize: 10,
                    },
                }
            }}
        />
    );
};

export default DrinkStatsPerDrinkerGraph;