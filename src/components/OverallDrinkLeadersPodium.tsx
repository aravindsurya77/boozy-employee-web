import React from "react";
import { DrinkerStatsRecord } from "./Leaderboard";
import {common, lightBlue, lime, yellow} from "@mui/material/colors";
import {Paper, Stack} from "@mui/material";
import { styled } from '@mui/material/styles';

const Item = styled(Paper)(({ theme, color }) => ({
    backgroundColor: color,
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: common.white,
    fontSize: 15,
    fontWeight: 'bold',
    flexGrow: 1,
}));

interface PodiumStepPros {
    height: number;
    color: string;
    numberOfDrinks: number;
    drinkerName: string;
    imageUrl?: string;
    rank: number;
}
const PodiumStep = ({ height, color, imageUrl, numberOfDrinks, drinkerName, rank }: PodiumStepPros) => {
    return (
        <div style={{display: "flex", flexDirection: "column",
            justifyContent: "center", alignItems: "center"}}>
            <span style={{ fontSize: 30, fontWeight: "bold" }}>{rank}</span>
            <Item color={color} style={{height: height, width: 75, marginLeft: 0}}>
                <div style={{display: 'flex', flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
                    <span>{numberOfDrinks}</span>
                    <span>{drinkerName}</span>
                </div>
            </Item>
        </div>
    );
};

interface OverallDrinkLeadersPodiumProps {
    top3Drinkers: Array<DrinkerStatsRecord>
}
const OverallDrinkLeadersPodium = ({ top3Drinkers }: OverallDrinkLeadersPodiumProps) => {
    const firstRank = top3Drinkers[0].totalDrinksForDrinker;
    const secondRank = top3Drinkers[1].totalDrinksForDrinker;
    const thirdRank = top3Drinkers[2].totalDrinksForDrinker;

    const heightOfFirstRank = 225;
    const heightOfSecondRank = (secondRank/firstRank) * heightOfFirstRank;
    const heightOfThirdRank = (thirdRank/firstRank) * heightOfFirstRank;

    return (
        <div style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center", alignItems: "flex-end",
            height: 250, margin: 30
        }}>
            <PodiumStep rank={2} height={heightOfSecondRank} color={lightBlue[600]} numberOfDrinks={top3Drinkers[1].totalDrinksForDrinker} drinkerName={top3Drinkers[1].drinkerName} />
            <PodiumStep rank={1} height={heightOfFirstRank} color={yellow[800]} numberOfDrinks={top3Drinkers[0].totalDrinksForDrinker} drinkerName={top3Drinkers[0].drinkerName} />
            <PodiumStep rank={3} height={heightOfThirdRank} color={lime[500]} numberOfDrinks={top3Drinkers[2].totalDrinksForDrinker} drinkerName={top3Drinkers[2].drinkerName}/>
        </div>
    );
};

export default OverallDrinkLeadersPodium;