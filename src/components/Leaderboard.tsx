import React from "react";
import { TapIds } from "./DrinksList";
import DrinkConsumptionPerTapGraph from "./DrinkConsumptionPerTapGraph";
import DrinkStatsPerDrinkerGraph from "./DrinkStatsPerDrinkerGraph";
import { Drinker, DrinkRecord } from "./NavigationTabs";
import OverallDrinkLeadersPodium from "./OverallDrinkLeadersPodium";

export interface TapConsumptionRecord {
    tapId: string;
    count: number;
}


export interface DrinkerStatsRecord {
    drinkerName: string;
    drinkerId: string;
    totalDrinksForDrinker: number;
    [TapIds.SECOND_FLOOR_BEER_TAP]: number;
    [TapIds.SECOND_FLOOR_WINE_TAP]: number;
    [TapIds.THIRD_FLOOR_TAP_1_LAGER]: number;
    [TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW]: number;
    [TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW]: number;
    [TapIds.THIRD_FLOOR_TAP_4_GUINNESS]: number;
    [TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE]: number;
    [TapIds.THIRD_FLOOR_TAP_6_RED_WINE]: number;
    [TapIds.SCOTCH_GLASS]: number;
}


const getTapConsumptionForTapId = (drinkRecords: Array<DrinkRecord>, tapId: string ): TapConsumptionRecord => {
    const filteredRecords = drinkRecords.filter(drinkRecord => drinkRecord.tapId === tapId);
    return {
        tapId,
        count: filteredRecords.length,
        // tapName: TapNames[tapId]
    }
}

const getDrinkerStatsPerTap = (tapId: string, drinkerId: string, drinkRecords: Array<DrinkRecord>): number => {
    const filteredDrinksPerTapForDrinker = drinkRecords.filter(drinkRecord => drinkRecord.userId === drinkerId && drinkRecord.tapId === tapId);
    return filteredDrinksPerTapForDrinker.length;
}

export const getDrinkerStats = (drinkRecords: Array<DrinkRecord>, drinkers: Array<Drinker>, drinkerId: string ): DrinkerStatsRecord => {
    const filteredRecordsForDrinker = drinkRecords.filter(drinkRecord => drinkRecord.userId === drinkerId);
    const drinkerDetails = drinkers.find(drinker => drinker.userId === drinkerId);
    return {
        drinkerId,
        // TODO: so that the key for the graph is always present
        drinkerName: drinkerDetails ? `${drinkerDetails.firstName}` : 'Bruno Mars',
        totalDrinksForDrinker: filteredRecordsForDrinker.length,
        [TapIds.SECOND_FLOOR_BEER_TAP]: getDrinkerStatsPerTap(TapIds.SECOND_FLOOR_BEER_TAP, drinkerId, filteredRecordsForDrinker),
        [TapIds.SECOND_FLOOR_WINE_TAP]: getDrinkerStatsPerTap(TapIds.SECOND_FLOOR_WINE_TAP, drinkerId, filteredRecordsForDrinker),
        [TapIds.THIRD_FLOOR_TAP_1_LAGER]: getDrinkerStatsPerTap(TapIds.THIRD_FLOOR_TAP_1_LAGER, drinkerId, filteredRecordsForDrinker),
        [TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW]: getDrinkerStatsPerTap(TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW, drinkerId, filteredRecordsForDrinker),
        [TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW]: getDrinkerStatsPerTap(TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW, drinkerId, filteredRecordsForDrinker),
        [TapIds.THIRD_FLOOR_TAP_4_GUINNESS]: getDrinkerStatsPerTap(TapIds.THIRD_FLOOR_TAP_4_GUINNESS, drinkerId, filteredRecordsForDrinker),
        [TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE]: getDrinkerStatsPerTap(TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE, drinkerId, filteredRecordsForDrinker),
        [TapIds.THIRD_FLOOR_TAP_6_RED_WINE]: getDrinkerStatsPerTap(TapIds.THIRD_FLOOR_TAP_6_RED_WINE, drinkerId, filteredRecordsForDrinker),
        [TapIds.SCOTCH_GLASS]: getDrinkerStatsPerTap(TapIds.SCOTCH_GLASS, drinkerId, filteredRecordsForDrinker),

    }
}

interface LeaderboardProps {
    drinkRecords: Array<DrinkRecord>;
    drinkers: Array<Drinker>;
}

const Leaderboard = ({drinkRecords, drinkers}: LeaderboardProps) => {

    const tapConsumptionRecords: Array<TapConsumptionRecord> = [
        getTapConsumptionForTapId(drinkRecords, TapIds.SECOND_FLOOR_BEER_TAP),
        getTapConsumptionForTapId(drinkRecords, TapIds.SECOND_FLOOR_WINE_TAP),
        getTapConsumptionForTapId(drinkRecords, TapIds.THIRD_FLOOR_TAP_1_LAGER),
        getTapConsumptionForTapId(drinkRecords, TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW),
        getTapConsumptionForTapId(drinkRecords, TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW),
        getTapConsumptionForTapId(drinkRecords, TapIds.THIRD_FLOOR_TAP_4_GUINNESS),
        getTapConsumptionForTapId(drinkRecords, TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE),
        getTapConsumptionForTapId(drinkRecords, TapIds.THIRD_FLOOR_TAP_6_RED_WINE),
        getTapConsumptionForTapId(drinkRecords, TapIds.SCOTCH_GLASS),
    ];

    const drinkStatsPerDrinker = drinkers.map(drinker => getDrinkerStats(drinkRecords, drinkers, drinker.userId));

    const top3Drinkers = drinkStatsPerDrinker
        .sort((a,b) => b.totalDrinksForDrinker - a.totalDrinksForDrinker)
        .slice(0, 3);

    return (
        <div id="leaderboard-stats" style={{ display: "flex", flexDirection: "column" }}>
            <div style={{ border: '5px solid black', marginTop: '10px' }}>
                <span style={{ marginLeft: '10px', fontStyle: 'bold', fontSize: '20px' }}>Total drinks leaders</span>
                <OverallDrinkLeadersPodium top3Drinkers={top3Drinkers}/>
            </div>
            <div style={{ border: '5px solid black', marginTop: '10px' }}>
                <span style={{ marginLeft: '10px', fontStyle: 'bold', fontSize: '20px' }}>Drink consumption per tap</span>
                <DrinkConsumptionPerTapGraph tapConsumptionRecords={tapConsumptionRecords}/>
            </div>
            <div style={{ border: '5px solid black', marginTop: '10px' }}>
                <span style={{ marginLeft: '10px', fontStyle: 'bold', fontSize: '20px' }}>Drink stats per person per tap</span>
                <DrinkStatsPerDrinkerGraph drinkStatsPerDrinker={drinkStatsPerDrinker}/>
            </div>
        </div>

    );
};

export default Leaderboard;