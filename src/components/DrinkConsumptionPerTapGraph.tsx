import React from "react";
import { TapConsumptionRecord } from "./Leaderboard";
import {TapIds} from "./DrinksList";
import { pieArcLabelClasses, PieChart } from "@mui/x-charts/PieChart";
import { brown, deepOrange, green, lightBlue, orange, pink, purple, red, yellow } from "@mui/material/colors";

interface DrinkConsumptionPerTapGraphProps {
    tapConsumptionRecords: Array<TapConsumptionRecord>
}

const DrinkConsumptionPerTapGraph = ({ tapConsumptionRecords }: DrinkConsumptionPerTapGraphProps) => {

    // @ts-ignore
    const dataForChart = tapConsumptionRecords.map(tapConsumptionRecord => {
        let tapLabel = '', color = '';
        switch (tapConsumptionRecord.tapId) {
            case TapIds.SECOND_FLOOR_BEER_TAP:
                tapLabel = '2F Beer Tap';
                color = purple[500];
                break;
            case TapIds.SECOND_FLOOR_WINE_TAP:
                tapLabel = '2F Wine Tap';
                color = lightBlue[700];
                break;
            case TapIds.THIRD_FLOOR_TAP_1_LAGER:
                tapLabel = '3F Tap1 Lager';
                color = orange[500];
                break;
            case TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW:
                tapLabel = '3F Tap2 Local Brew';
                color = green[500];
                break;
            case TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW:
                tapLabel = '3F Tap3 Local Brew';
                color = deepOrange[500];
                break;
            case TapIds.THIRD_FLOOR_TAP_4_GUINNESS:
                tapLabel = '3F Tap4 Guinness';
                color = brown[600];
                break;
            case TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE:
                tapLabel = '3F Tap5 White Wine';
                color = pink[400];
                break;
            case TapIds.THIRD_FLOOR_TAP_6_RED_WINE:
                tapLabel = '3F Tap6 Red Wine';
                color = red[900];
                break;
            case TapIds.SCOTCH_GLASS:
                tapLabel = 'Personal Scotch';
                color = yellow[700];
                break;
        }

        return {
            id: tapConsumptionRecord.tapId,
            value: tapConsumptionRecord.count,
            // @ts-ignore
            label: tapLabel,
            color,
        };

    });

    const chartSetting = {
        margin: { top: 80, bottom: 50, left: 17 }
    };

    return (
        <PieChart
            series={[{
                // @ts-ignore
                data: dataForChart,
                highlightScope: { faded: 'global', highlighted: 'item' },
                faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
                arcLabel: (item) => `${item.value}`,
                arcLabelMinAngle: 25,
                arcLabelRadius: 75,
                innerRadius: 14,
                outerRadius: 150,
                paddingAngle: 1,
                cornerRadius: 5,
                startAngle: -180,
                endAngle: 180,
                cx: 150,
                cy: 150,
            }]}
            sx={{
                [`& .${pieArcLabelClasses.root}`]: {
                    fill: 'white',
                    fontWeight: 'bold',
                },
            }}
            slotProps={{
                legend: {
                    direction: 'row',
                    position: { vertical: 'top', horizontal: 'middle' },
                    padding: 0,
                    itemMarkWidth: 5,
                    itemMarkHeight: 5,
                    markGap: 5,
                    itemGap: 5,
                    labelStyle: {
                        fontSize: 10,
                    },
                }
            }}
            width={350}
            height={400}
            {...chartSetting}
        />
    );
};

export default DrinkConsumptionPerTapGraph;