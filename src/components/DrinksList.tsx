import * as React from 'react';
import SportsBarIcon from '@mui/icons-material/SportsBar';
import SportsBarOutlinedIcon from '@mui/icons-material/SportsBarOutlined';
import WineBarOutlinedIcon from '@mui/icons-material/WineBarOutlined';
import WineBarIcon from '@mui/icons-material/WineBar';
import LiquorIcon from '@mui/icons-material/Liquor';
import LiquorOutlinedIcon from '@mui/icons-material/LiquorOutlined';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { yellow, red, brown, deepOrange, grey } from '@mui/material/colors';
import Button from '@mui/material/Button';
import {FIREBASE_AUTH, FIREBASE_DB} from "../FirebaseConfig";
import { serverTimestamp, setDoc, doc } from "firebase/firestore";
import {v4 as uuidv4} from "uuid";
import {useState} from "react";


export enum TapIds {
    THIRD_FLOOR_TAP_1_LAGER = "3f-tap1-lager",
    THIRD_FLOOR_TAP_2_LOCAL_BREW = "3f-tap2-local-brew",
    THIRD_FLOOR_TAP_3_LOCAL_BREW = "3f-tap3-local-brew",
    THIRD_FLOOR_TAP_4_GUINNESS = "3f-tap4-guinness",
    THIRD_FLOOR_TAP_5_WHITE_WINE = "3f-tap5-white-wine",
    THIRD_FLOOR_TAP_6_RED_WINE = "3f-tap6-red-wine",
    SECOND_FLOOR_BEER_TAP = "2f-beer-tap",
    SECOND_FLOOR_WINE_TAP = "2f-wine-tap",
    SCOTCH_GLASS = "scotch-glass",
}

/*export enum TapNames {
    THIRD_FLOOR_TAP_1_LAGER = "3F Tap1 Lager",
    THIRD_FLOOR_TAP_2_LOCAL_BREW = "3F Tap2 Local Brew",
    THIRD_FLOOR_TAP_3_LOCAL_BREW = "3F Tap3 Local Brew",
    THIRD_FLOOR_TAP_4_GUINNESS = "3F Tap4 Guinness",
    THIRD_FLOOR_TAP_5_WHITE_WINE = "3F Tap5 White Wine",
    THIRD_FLOOR_TAP_6_RED_WINE = "3F Tap6 Red Wine",
    SECOND_FLOOR_BEER_TAP = "2F Beer Tap",
    SECOND_FLOOR_WINE_TAP = "2F Wine Tap",
    SCOTCH_GLASS = "Personal Scotch",
}*/

const tapsConfig = [
    {
        tapId: TapIds.THIRD_FLOOR_TAP_1_LAGER,
        tapName: "3F Tap1 Lager",
        unCheckedIcon: <SportsBarOutlinedIcon />,
        checkedIcon:<SportsBarIcon />,
        selectedColor: yellow[800]
    },
    {
        tapId: TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW,
        tapName: "3F Tap2 Local Brew",
        unCheckedIcon: <SportsBarOutlinedIcon />,
        checkedIcon:<SportsBarIcon />,
        selectedColor: yellow[800]
    },
    {
        tapId: TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW,
        tapName: "3F Tap3 Local Brew",
        unCheckedIcon: <SportsBarOutlinedIcon />,
        checkedIcon:<SportsBarIcon />,
        selectedColor: yellow[800]
    },
    {
        tapId: TapIds.THIRD_FLOOR_TAP_4_GUINNESS,
        tapName: "3F Tap4 Guinness",
        unCheckedIcon: <SportsBarOutlinedIcon />,
        checkedIcon:<SportsBarIcon />,
        selectedColor: brown[800]
    },
    {
        tapId: TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE,
        tapName: "3F Tap5 White Wine",
        unCheckedIcon: <WineBarOutlinedIcon />,
        checkedIcon:<WineBarIcon />,
        selectedColor: deepOrange[500]
    },
    {
        tapId: TapIds.THIRD_FLOOR_TAP_6_RED_WINE,
        tapName: "3F Tap6 Red Wine",
        unCheckedIcon: <WineBarOutlinedIcon />,
        checkedIcon:<WineBarIcon />,
        selectedColor: red[900]
    },
    {
        tapId: TapIds.SECOND_FLOOR_BEER_TAP,
        tapName: "2F Beer Tap",
        unCheckedIcon: <SportsBarOutlinedIcon />,
        checkedIcon:<SportsBarIcon />,
        selectedColor: yellow[800]
    },
    {
        tapId: TapIds.SECOND_FLOOR_WINE_TAP,
        tapName: "2F Wine Tap",
        unCheckedIcon: <WineBarOutlinedIcon />,
        checkedIcon:<WineBarIcon />,
        selectedColor: deepOrange[500]
    },
    {
        tapId: TapIds.SCOTCH_GLASS,
        tapName: "Personal Scotch",
        unCheckedIcon: <LiquorOutlinedIcon />,
        checkedIcon:<LiquorIcon />,
        selectedColor: yellow["A700"]
    },
];

interface TapSelectionState {
    [TapIds.THIRD_FLOOR_TAP_1_LAGER]: boolean,
    [TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW]: boolean,
    [TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW]: boolean,
    [TapIds.THIRD_FLOOR_TAP_4_GUINNESS]: boolean,
    [TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE]: boolean,
    [TapIds.THIRD_FLOOR_TAP_6_RED_WINE]: boolean,
    [TapIds.SECOND_FLOOR_BEER_TAP]: boolean,
    [TapIds.SECOND_FLOOR_WINE_TAP]: boolean,
    [TapIds.SCOTCH_GLASS]: boolean,
}

const tapSelectionInitialState = {
    [TapIds.THIRD_FLOOR_TAP_1_LAGER]: false,
    [TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW]: false,
    [TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW]: false,
    [TapIds.THIRD_FLOOR_TAP_4_GUINNESS]: false,
    [TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE]: false,
    [TapIds.THIRD_FLOOR_TAP_6_RED_WINE]: false,
    [TapIds.SECOND_FLOOR_BEER_TAP]: false,
    [TapIds.SECOND_FLOOR_WINE_TAP]: false,
    [TapIds.SCOTCH_GLASS]: false,
}


export default function CheckboxLabels() {
    const [tapSelection, setTapSelectionState] = useState<TapSelectionState>(tapSelectionInitialState);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTapSelectionState({
            ...tapSelection,
            [event.target.id]: event.target.checked,
        });
    };

    const logDrinks = async () => {
        const currentUserId = FIREBASE_AUTH.currentUser?.uid;
        const drankAt = serverTimestamp();
        const drinkRecords = Object.keys(tapSelection).map(key => {
            // @ts-ignore
            if (tapSelection[key] === true) {
                // build and return drink record
                return {
                    drinkRecordId: uuidv4(),
                    tapId: key,
                    userId: currentUserId,
                    timestamp: drankAt,
                }
            }
        });

        drinkRecords.filter(drinkRecord => !!drinkRecord)
            .forEach(async (drinkRecord) => {
                // @ts-ignore
                await setDoc(doc(FIREBASE_DB, "drinks", drinkRecord.drinkRecordId), drinkRecord)
            });
        alert('Logged your drinks');
        setTapSelectionState(tapSelectionInitialState);
    };

    const isSubmitLogDisabled = !tapSelection[TapIds.SCOTCH_GLASS] && 
    !tapSelection[TapIds.SECOND_FLOOR_BEER_TAP] && 
    !tapSelection[TapIds.SECOND_FLOOR_WINE_TAP] && 
    !tapSelection[TapIds.THIRD_FLOOR_TAP_1_LAGER] && 
    !tapSelection[TapIds.THIRD_FLOOR_TAP_2_LOCAL_BREW] && 
    !tapSelection[TapIds.THIRD_FLOOR_TAP_3_LOCAL_BREW] && 
    !tapSelection[TapIds.THIRD_FLOOR_TAP_4_GUINNESS] && 
    !tapSelection[TapIds.THIRD_FLOOR_TAP_5_WHITE_WINE] && 
    !tapSelection[TapIds.THIRD_FLOOR_TAP_6_RED_WINE] 

    return (
        <FormGroup>
            {
                tapsConfig.map(tap => (
                    <FormControlLabel
                        control={
                            <Checkbox
                                id={tap.tapId}
                                icon={tap.unCheckedIcon}
                                checkedIcon={tap.checkedIcon}
                                onChange={handleChange}
                                // @ts-ignore
                                checked={tapSelection[tap.tapId]}
                                sx={{
                                    color: tap.selectedColor,
                                    '&.Mui-checked': {
                                        color: tap.selectedColor,
                                    },
                                }}
                            />}
                        label={tap.tapName}
                        sx={{
                            backgroundColor: tapSelection[tap.tapId] ? grey[300] : ''
                        }}
                    />
                ))
            }
            <div style={{ marginTop: '40px', display: "flex", justifyContent: "center", alignItems: "center" }}>
                <Button title="Submit Log" variant="contained" onClick={logDrinks} disabled={isSubmitLogDisabled}>Log Drinks</Button>
            </div>
        </FormGroup>
    );
}