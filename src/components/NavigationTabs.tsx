import React, { useState, useEffect } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import DrinksList, {TapIds} from "./DrinksList";
import Profile from "./Profile";
import Leaderboard, { getDrinkerStats } from './Leaderboard';
import { collection, doc, getDoc, getDocs, query, where } from "firebase/firestore";
import { FIREBASE_AUTH, FIREBASE_DB } from "../FirebaseConfig";
import DateRangeSelector from "./DateRangeSelector";
import dayjs, { Dayjs } from 'dayjs';
import { Timestamp } from "firebase/firestore";

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

type ValueOf<T> = T[keyof T];
type ValueOfTapIds = ValueOf<TapIds>


export interface DrinkRecord {
    timestamp: Date;
    userId: string;
    tapId: ValueOfTapIds;
    drinkRecordId: string;
}

export interface Drinker {
    email: string;
    firstName: string;
    lastName: string;
    userId: string;
}

function CustomTabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function BasicTabs() {
    const [value, setValue] = useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const [currentDrinkerDetails, setCurrentDrinkerDetails] = useState(null);
    const currentUserId = FIREBASE_AUTH.currentUser?.uid;

    const [drinkRecords, setDrinkRecords] = useState<Array<DrinkRecord>>([]);
    const [drinkers, setDrinkers] = useState<Array<Drinker>>([]);

    const [startDate, setStartDate] = useState<Dayjs>(dayjs('2024-02-28'));
    const [endDate, setEndDate] = useState<Dayjs>(dayjs(new Date().toString()));
    const updateStartDate = (newDate: Dayjs) => setStartDate(newDate);
    const updateEndDate = (newDate: Dayjs) => setEndDate(newDate);

    useEffect(() => {
        getDrinkerDetails();
        getDrinkRecords();
        getDrinkers();
    }, [startDate, endDate]);

    const getDrinkerDetails = async () => {
        // @ts-ignore
        if (currentUserId != null) {
            const docRef = doc(FIREBASE_DB, "drinkers", currentUserId);
            const docSnap = await getDoc(docRef);
            if (docSnap.exists()) {
                // @ts-ignore
                setCurrentDrinkerDetails(docSnap.data());
            } else {
                // docSnap.data() will be undefined in this case
                console.log("No such document!");
            }
        }
    };

    // TODO: Eventually this function should account the date range.
    const getDrinkRecords = async () => {
        let drinksFromDb: Array<DrinkRecord> = [];

        const q = query(
            collection(FIREBASE_DB, "drinks"),
            where("timestamp", ">=", Timestamp.fromDate(startDate.toDate())),
            where("timestamp", "<=", Timestamp.fromDate(endDate.toDate())),
        );

        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
            drinksFromDb.push(doc.data() as DrinkRecord);
        });
        setDrinkRecords(drinksFromDb);
    };

    const getDrinkers = async () => {
        let drinkersFromDb: Array<Drinker> = [];
        const querySnapshot = await getDocs(collection(FIREBASE_DB, "drinkers"));
        querySnapshot.forEach((doc) => {
            drinkersFromDb.push(doc.data() as Drinker);
        });
        setDrinkers(drinkersFromDb);
    };

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" indicatorColor="secondary"
                      textColor="inherit"
                      variant="fullWidth">
                    <Tab label="Log Drink" {...a11yProps(0)} />
                    <Tab label="Leaderboard" {...a11yProps(1)} />
                    <Tab label="Profile" {...a11yProps(2)} />
                </Tabs>
            </Box>
            <CustomTabPanel value={value} index={0}>
                <DrinksList/>
            </CustomTabPanel>
            <CustomTabPanel value={value} index={1}>
                <DateRangeSelector updateStartDate={updateStartDate} updateEndDate={updateEndDate} endDate={endDate} startDate={startDate}/>
                <Leaderboard drinkers={drinkers} drinkRecords={drinkRecords}/>
            </CustomTabPanel>
            <CustomTabPanel value={value} index={2}>
                <DateRangeSelector updateStartDate={updateStartDate} updateEndDate={updateEndDate} endDate={endDate} startDate={startDate}/>
                <Profile
                    // @ts-ignore
                    drinkerDetails={currentDrinkerDetails}
                    // @ts-ignore
                    drinkerStats={getDrinkerStats(drinkRecords, drinkers, currentUserId)}
                />
            </CustomTabPanel>
        </Box>
    );
}