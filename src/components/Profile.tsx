import React from 'react'
import Button from '@mui/material/Button';
import { FIREBASE_AUTH } from "../FirebaseConfig";
import LogoutIcon from '@mui/icons-material/Logout';
import MyDrinksStatsChart from "./MyDrinkStatsChart";
import { Drinker } from "./NavigationTabs";
import { DrinkerStatsRecord } from "./Leaderboard";

interface ProfileProps {
    drinkerStats: DrinkerStatsRecord;
    drinkerDetails: Drinker
}

const Profile = ({drinkerStats, drinkerDetails}: ProfileProps) => {
    return (
        <div style={{display: 'flex', flexDirection: 'column'}}>
            {/* @ts-ignore */}
            Hello {drinkerDetails.firstName}! Here is your drink consumption.

            <div style={{ border: '5px solid black', marginBottom: '50px', marginTop: '10px' }}>
                <MyDrinksStatsChart drinkerStats={drinkerStats}/>
            </div>


            <Button onClick={() => FIREBASE_AUTH.signOut()} variant="contained" title="Logout" startIcon={<LogoutIcon/>}>Logout</Button>
        </div>
    )
}
export default Profile;