// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAa1sz-AWNBuiQFXj_JfA1nQP2QJyqAT64",
    authDomain: "boozyemployee.firebaseapp.com",
    projectId: "boozyemployee",
    storageBucket: "boozyemployee.appspot.com",
    messagingSenderId: "976085696999",
    appId: "1:976085696999:web:f789e5959bd71e70a87d42"
};

// Initialize Firebase
export const FIREBASE_APP = initializeApp(firebaseConfig);
export const FIREBASE_AUTH = getAuth(FIREBASE_APP);
export const FIREBASE_DB = getFirestore(FIREBASE_APP);
