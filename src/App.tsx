import { useEffect, useState } from 'react';
import { User, onAuthStateChanged } from 'firebase/auth';
import { FIREBASE_AUTH } from './FirebaseConfig';
import NavigationTabs from "./components/NavigationTabs";
import Login from "./screens/Login";


const App = () => {
    const [user, setUser] = useState<User | null>(null);

    useEffect(() => {
        onAuthStateChanged(FIREBASE_AUTH, (user) => {
            setUser(user);
        });
    }, []);

    return (
    <div>
            {
                !user ? <Login/>: <NavigationTabs/>

            }
    </div>
  );
}

export default App;
