import React, { useState } from 'react'
import {FIREBASE_AUTH, FIREBASE_DB} from '../FirebaseConfig';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { CircularProgress } from "@mui/material";
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import Box from "@mui/material/Box";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { doc, setDoc } from "firebase/firestore";

interface LoginScreenProps {
    showLoginScreen: () => void
}
const Login = ({showLoginScreen}: LoginScreenProps) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [inviteCode, setInviteCode] = useState('');
    const [loading, setLoading] = useState(false);
    const auth = FIREBASE_AUTH;

    // @ts-ignore
    const createUserDetails = async (userId) => {
        // @ts-ignore
        await setDoc(doc(FIREBASE_DB, "drinkers", userId), {
            firstName,
            lastName,
            userId,
            email
        })
    }

    const signUpFunction = async () => {
        setLoading(true);
        try {
            const response = await createUserWithEmailAndPassword(auth, email, password);
            console.log(response);
            await createUserDetails(response.user.uid);
            alert(`Welcome ${firstName}! Please log your drinks.`);
        } catch (error) {
            console.log(error);
            alert('Registration failed');
        } finally {
            setLoading(false);
        }
    };

    const inviteCodeIsIncorrect = inviteCode !== "99YALER_LABOLG91";
    const isButtonDisabled = !(!!email && !!password && !!firstName && !!lastName) || inviteCodeIsIncorrect;

    return (
        <div style={{display: 'flex', flexDirection: 'column', padding: '20px'}}>
            <div style={{ display: 'flex', flexDirection: 'row', paddingBottom: '10px', justifyContent: 'center', alignItems: 'center', fontSize: '30px', fontWeight: 'bold' }}>
                <img src="/drinking.png" alt="drinking" width={50} height={50} />
                Boozy Employee
            </div>
            <TextField placeholder='First Name *' autoCapitalize="none" onChange={(e) => setFirstName(e.target.value)} style={{ paddingBottom: '10px' }} />
            <TextField placeholder='Last Name *' autoCapitalize="none" onChange={(e) => setLastName(e.target.value)} style={{ paddingBottom: '10px' }} />
            <TextField inputMode="email" placeholder='Email *' autoCapitalize="none" onChange={(e) => setEmail(e.target.value)} style={{ paddingBottom: '10px' }} />
            <TextField
                type="password"
                placeholder='Password *'
                autoCapitalize="none"
                onChange={(e) => setPassword(e.target.value)}
                style={{ paddingBottom: '10px' }}
            />
            <TextField
                placeholder='Invite Code *'
                autoCapitalize="none"
                onChange={(e) => setInviteCode(e.target.value)}
                style={{ paddingBottom: '10px' }}
                error={inviteCodeIsIncorrect && !!inviteCode}
            />
            {
                loading ?
                    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                        <CircularProgress />
                    </Box>
                    :
                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        <Button title="Sign Up" variant="contained" onClick={signUpFunction} disabled={isButtonDisabled}>Sign Up</Button>
                        <Button title="Back" variant="contained" onClick={showLoginScreen} startIcon={<ArrowBackIosNewIcon/>}>Back</Button>
                    </div>
            }

        </div>
    )
}

export default Login
