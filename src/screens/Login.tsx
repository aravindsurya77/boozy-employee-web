import React, { useState } from 'react'
import { FIREBASE_AUTH } from '../FirebaseConfig';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { CircularProgress } from "@mui/material";
import Box from "@mui/material/Box";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import SingUp from './SingUp';

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [showSignUpScreen, setShowSignUpScreen] = useState(false);
    const auth = FIREBASE_AUTH;


    const loginFunction = async () => {
        setLoading(true);
        try {
            const response = await signInWithEmailAndPassword(auth, email, password);
            console.log(response);
        } catch (error) {
            console.log(error);
            alert('Sign in failed');
        } finally {
            setLoading(false);
        }
    };

    const showLoginScreen = () => setShowSignUpScreen(false);

    return (
        <>
            {
                showSignUpScreen ? 
                    <SingUp showLoginScreen={showLoginScreen}/> :
                    <div style={{ display: 'flex', flexDirection: 'column', padding: '20px' }}>
                        <div style={{ display: 'flex', flexDirection: 'row', paddingBottom: '10px', justifyContent: 'center', alignItems: 'center', fontSize: '30px', fontWeight: 'bold' }}>
                            <img src="/drinking.png" alt="drinking" width={50} height={50} />
                            Boozy Employee
                        </div>
                        <TextField inputMode="email" placeholder='Email' autoCapitalize="none" onChange={(e) => setEmail(e.target.value)} style={{ paddingBottom: '10px' }} />
                        <TextField type="password" placeholder='Password' autoCapitalize="none" onChange={(e) => setPassword(e.target.value)} style={{ paddingBottom: '10px' }} />
                        {
                            loading ?
                                <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                                    <CircularProgress />
                                </Box>
                                :
                                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly'}}>
                                    <Button title="Login" variant="contained" onClick={loginFunction}>Login</Button>
                                    <Button title="Sign Up" variant="contained" onClick={() => setShowSignUpScreen(true)}>Sign Up</Button>
                                </div>
                        }
                    </div>
            }
        </>
    )
}

export default Login
